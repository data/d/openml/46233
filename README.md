# OpenML dataset: Tourism-competition-yearly

https://www.openml.org/d/46233

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Tourism competion for time series forecasting, yearly data.

From original source:
-----
The data we use include 366 monthly series, 427 quarterly series and 518 yearly series. They were supplied by both tourism bodies (such as Tourism Australia, the Hong Kong Tourism Board and Tourism New Zealand) and various academics, who had used them in previous tourism forecasting studies (please refer to the acknowledgements and details of the data sources and availability).

A subset of these series was used for evaluating the forecasting performances of the methods that use explanatory variables. There were 93 quarterly series and 129 yearly series for which we had explanatory variables available. With the exception of 34 yearly series (which represented tourism numbers by purpose of travel at a national level), all of the other series represented total tourism numbers at a country level of aggregation.

For each series we split the data into an estimation sample and a hold-out sample which was hidden from all of the co-authors. For each monthly series, the hold-out sample consisted of the 24 most recent observations; for quarterly data, it was the last 8 observations; and for yearly data it consisted of the final 4 observations. Each method was implemented (or trained) on the estimation sample, and forecasts were produced for the whole of the hold-out sample for each series. The forecasts were then compared to the actual withheld observations.
-----

There are 4 columns:

id_series: The id of the time series.

date: The date of the time series in the format "%Y-%m-%d".

time_step: The time step on the time series.

value_0: The values of the time series, which will be used for the forecasting task.

Preprocessing:

Training (in) set

1 - Renamed first two columns to 'n' and 'starting_year', and renamed the other columns to reflect the actual time_step of the time series.

2 - Melted the data, obtaining columns 'time_step' and 'value_0'.

3 - Dropped nan values.

The nan values correspond to time series that are shorter than the time series with maximum lenght, there are no nans in the middle of a time series.

3 - Obtained the 'date' from the 'starting_year' and 'time_step'.

4 - Casted 'date' to str, 'time_step' to int, 'value_0' to float, and defined 'id_series' as 'category'.

Test (oos) set:

Same as for the training set. 

Finally, we have concatenated both training and test set. If one wants to use the same train and test set of the competition, the last N points should be
used as test set, where N is 24 for the montthly dataset, 8 for the quarterly dataset and 4 for the yearly dataset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46233) of an [OpenML dataset](https://www.openml.org/d/46233). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46233/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46233/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46233/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

